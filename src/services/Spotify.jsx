import axios from 'axios';

const credentials = {
  clientId: process.env.REACT_APP_SPOTIFY_CLIENT_ID,
  callbackHost: process.env.REACT_APP_SPOTIFY_CALLBACK_HOST,
  scopes : ['user-library-modify','user-library-read','user-read-recently-played']
}

const authURL = 'https://accounts.spotify.com/es-ES/authorize?client_id='+
  credentials.clientId + '&response_type=token'+
  '&redirect_uri=' + credentials.callbackHost+
  '&scope='+ credentials.scopes.join("%20") +
  '&expires_in=3600';

const getToken = () => {
  window.location.replace(authURL);
};

const saveToken = (tk) => {
  const aux = tk.split('&').map(val=> {return val.split('=')});
  localStorage.setItem('token',aux[0][1]);
  localStorage.setItem('type',aux[1][1]);
  localStorage.setItem('expires',aux[2][1]);
}

const removeToken = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('type');
  localStorage.removeItem('expires');
}

const checkToken = () => {
  const userToken = localStorage.getItem('token');

  var isExpired = userToken !== null;
  console.log(isExpired);
  if(!(isExpired)) {
    removeToken();
  }
  return isExpired;
}

const checkIsFavorite = async ( songParams ) => {
  return axios({
    url: `${process.env.REACT_APP_SPOTIFY_ENDPOINT}/tracks/contains?ids=${songParams}`,
    method: 'get',
    headers: {
      'Authorization': `${localStorage.getItem('type')} ${localStorage.getItem('token')}`,
      'Retry-after':40
    }
  })
}

const getRecentlyPlayed = async () => {
  return axios({
    url: `${process.env.REACT_APP_SPOTIFY_ENDPOINT}/player/recently-played`,
    method: 'get',
    headers: {
      'Authorization': `${localStorage.getItem('type')} ${localStorage.getItem('token')}`,
      'Retry-after':40
    }
  });
}

const getFavoriteSongs = async () => {
  return axios({
    url: `${process.env.REACT_APP_SPOTIFY_ENDPOINT}/tracks`,
    method: 'get',
    headers: {
      'Authorization': `${localStorage.getItem('type')} ${localStorage.getItem('token')}`,
      'Retry-after':40
    }
  });
}

const getUserData = async () => {
  return axios({
    url: `${process.env.REACT_APP_SPOTIFY_ENDPOINT}`,
    method: 'get',
    headers: {
      'Authorization': `${localStorage.getItem('type')} ${localStorage.getItem('token')}`,
      'Retry-after':40
    }
  });
}

const addSongFavorites = (songParam) => {
  axios({
    url: `${process.env.REACT_APP_SPOTIFY_ENDPOINT}/tracks?ids=${songParam}`,
    method: 'put',
    headers: {
      'Authorization': `${localStorage.getItem('type')} ${localStorage.getItem('token')}`,
      'Retry-after':40
    }
  });
}

const removeSongFavorites = (songParam) => {
  axios({
    url: `${process.env.REACT_APP_SPOTIFY_ENDPOINT}/tracks?ids=${songParam}`,
    method: 'delete',
    headers: {
      'Authorization': `${localStorage.getItem('type')} ${localStorage.getItem('token')}`,
      'Retry-after':40
    }
  });
}

export default {
  getToken,
  saveToken,
  removeToken,
  checkToken,
  checkIsFavorite,
  getRecentlyPlayed,
  getUserData,
  getFavoriteSongs,
  addSongFavorites,
  removeSongFavorites,
  credentials
};