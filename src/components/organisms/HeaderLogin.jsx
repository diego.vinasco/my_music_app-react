import React, { Component } from "react";
import './../../assets/styles/HeaderLogin.scss';

export default class HeaderLogin extends Component {
  render() {
    return (
      <header className="header-login">
        <img src={require('./../../assets/imgs/spotifyIcon.png')} className="header-login__img" alt="Spotify icon" />
      </header>
    )
  }
}