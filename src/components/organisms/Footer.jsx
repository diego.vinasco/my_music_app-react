import React, { Component } from "react";
import './../../assets/styles/Footer.scss';

export default class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <p>My Music App</p>
      </footer>
    )
  }
}