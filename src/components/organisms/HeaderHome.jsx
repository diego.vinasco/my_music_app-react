import React from "react";
import menuIcon from "./../../assets/imgs/menuIcon.svg";
import "./../../assets/styles/HeaderHome.scss";

//test img
import testImg from "./../../assets/imgs/exampleImg.PNG";
import { useState } from "react";
import { useEffect } from "react";
import Spotify from "../../services/Spotify";
import { useNavigate } from "react-router";

export default function HeaderHome () {
  const [userData, setUserData] = useState([]);
  const [userName, setUserName] = useState('');
  const [userImg, setUserImg] = useState('');

  const navigate = useNavigate();

  useEffect( () => {
    Spotify.getUserData()
      .then( res => {
        console.log(res);
        setUserData(res.data);
        setUserName(res.data.display_name);
        setUserImg(res.data.images[0].url);
      });
  }, []);
  
  const handleLogout = () => {
    Spotify.removeToken();
    navigate("/");
  }

  const goHome = () => {
    navigate("/home");
  }

  const goFavorites = () => {
    navigate("/favorites");
  }

  return (
    <header className="header-home">
      <a onClick={() => goHome()} className="header-home__a">
        <img src={require('./../../assets/imgs/spotifyIcon.png')} className="header-home__img" alt="Spotify icon"/>
      </a>
      
      <nav className="header-home__nav">
        <li className="nav__li">
          <div className="nav__li__title">
            <img src={menuIcon} className="nav__li__title__icon"/>
            <img src={userImg} className="nav__li__title__img"/>
            <p className="nav__li__title__a">{userName}</p>
          </div>
          <div className="dropdown">
            <a onClick={() => goFavorites()} className="dropdown__item">Favorites</a>
            <a onClick={() => handleLogout()} className="dropdown__item">Logout</a>
          </div>
        </li>
      </nav>
    </header>
  );
}