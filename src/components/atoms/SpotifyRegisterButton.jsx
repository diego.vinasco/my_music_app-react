import React, { Component } from "react";
import "./../../assets/styles/SpotifyRegisterButton.scss";

export default class SpotifyRegisterButton extends Component {

  onClick() {
    window.location.href = 'https://www.spotify.com/co/signup';
  }

  render() {
    return (
      <button onClick={() => { this.onClick() }} className="btn-register">Register</button>
    )
  }
}