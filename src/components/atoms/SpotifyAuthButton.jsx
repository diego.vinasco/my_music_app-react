import React, { Component } from "react";
import Spotify from "../../services/Spotify";
import "./../../assets/styles/SpotifyAuthButton.scss";

export default class SpotifyAuthButton extends Component {

  render() {
    return (
      <button onClick={() => {Spotify.getToken()}} className="btn-auth">Continue<br/>with Spotify</button>
    )
  }
}