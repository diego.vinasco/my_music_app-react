import React, { useState } from "react";
import { useEffect } from "react";
import Spotify from "../../services/Spotify";

import HeartIcon from "../atoms/HeartIcon";
import "./../../assets/styles/SongBox.scss";

export default function SongBox ({song, isFavorite}) {
  const [isSongFavorite, setIsSongFavorite] = useState(isFavorite);

  useEffect( () => {
    setIsSongFavorite(isFavorite)
  }, [isFavorite]);
  
  const songImg=song.track.album.images[1].url;
  const songName=song.track.name;
  const songArtist=song.track.artists[0].name;
  
  const addFavorite = () => {
    Spotify.addSongFavorites(song.track.id);
    setIsSongFavorite(true);
  }

  const removeFavorite = () => {
    Spotify.removeSongFavorites(song.track.id);
    setIsSongFavorite(false);
  }

  return (
    <div className="card">
      <img src={songImg} className="card__img" alt="song box image"/>
      <div className="card__container">
        <div className="card__container-info">
          <p className="card__container-info__p--title">{songName}</p>
          <p className="card__container-info__p--author">{songArtist}</p>
        </div>
        { isSongFavorite
            ? <a onClick={() => removeFavorite()} className="card__container__img">
                <HeartIcon width="20px" height="20px" fill="#1DB954"/>
              </a>
            : <a onClick={() => addFavorite()} className="card__container__img">
                <HeartIcon width="20px" height="20px" fill="white"/>
              </a>
        }
      </div>
    </div>
  )
}