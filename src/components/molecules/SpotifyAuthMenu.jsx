import React, { Component } from "react";
import SpotifyAuthButton from "../atoms/SpotifyAuthButton";
import SpotifyRegisterButton from "../atoms/SpotifyRegisterButton";
import "../../assets/styles/SpotifyAuthMenu.scss";

export default class SpotifyAuthMenu extends Component {
  render() {
    return (
      <div className="div-login">
        <SpotifyAuthButton/>
        <hr className="div-login__hr" />
        <p className="div-login__p">Or if you don't have an<br/>account</p>
        <SpotifyRegisterButton/>
      </div>
    )
  }
}