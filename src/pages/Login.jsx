import React, { useEffect } from "react";
import HeaderLogin from "../components/organisms/HeaderLogin";
import SpotifyAuthMenu from "../components/molecules/SpotifyAuthMenu";
import Footer from "../components/organisms/Footer";
import Spotify from "../services/Spotify";
import { useNavigate } from "react-router-dom";

export default function Login() {
  
  const URLToken = window.location.hash;
  const navigate = useNavigate();
  const isLogged = Spotify.checkToken();

  useEffect(() => {
    console.log(URLToken);
    if(URLToken) {
      Spotify.saveToken(URLToken);
      navigate("/home");
    }
  }, [URLToken]);

  useEffect(() => {
    if(isLogged) {
      navigate("/home");
    }
  }, [isLogged]);

  return (
    <React.Fragment>
      <HeaderLogin/>
      <SpotifyAuthMenu/>
      <Footer/>
    </React.Fragment>
  )
}