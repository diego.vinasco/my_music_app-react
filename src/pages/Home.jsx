import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router";
import SongBox from "../components/molecules/SongBox";
import Footer from "../components/organisms/Footer";
import HeaderHome from "../components/organisms/HeaderHome";
import Spotify from "../services/Spotify";
import "./../assets/styles/Home.scss";



export default function Home() {
  const [songs, setSongs] = useState([]);
  const [isFavorite, setIsFavorite] = useState([]);

  const navigate = useNavigate();
  const isLogged = Spotify.checkToken();

  //get which are the favorites songs in the array
  const updateIsFavoriteArray = (songParams) => {
    Spotify.checkIsFavorite(songParams)
      .then( res => {
        setIsFavorite(res.data);
      });
  }

  useEffect( () => {
    Spotify.getRecentlyPlayed()
      .then( res => {
        setSongs(res.data.items);

        var songParams = '';
        for(let aux of res.data.items) {
          songParams += aux.track.id + ',';
        }
        songParams = songParams.slice(0,-1);
        
        updateIsFavoriteArray(songParams);
      });
    }, []);
  console.log(songs);
  console.log(isFavorite);

  useEffect( () => {
    if(!(isLogged)) {
      navigate("/");
    }
  }, [isLogged] );

  return (
    <React.Fragment>
      <HeaderHome/>
      <main className="main-home">
        {songs.map((song, i) => {
          return(
            <SongBox
              key={i}
              song={song}
              isFavorite={isFavorite[i]}
            />
          )
        })}
      </main>
      <Footer/>
    </React.Fragment>
  )
}