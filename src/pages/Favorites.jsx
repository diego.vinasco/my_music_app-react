import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router";
import SongBox from "../components/molecules/SongBox";
import Footer from "../components/organisms/Footer";
import HeaderHome from "../components/organisms/HeaderHome";
import Spotify from "../services/Spotify";
import "./../assets/styles/Favorites.scss";

export default function Favorites () {
  const [songs, setSongs] = useState([]);
  
  const navigate = useNavigate();
  const isLogged = Spotify.checkToken();

  useEffect( () => {
    Spotify.getFavoriteSongs()
      .then( res => {
        setSongs(res.data.items);
      });
  }, []);
  console.log(songs);

  useEffect( () => {
    if(!(isLogged)) {
      navigate("/");
    }
  }, [isLogged] );


  return (
    <React.Fragment>
      <HeaderHome/>
      <p className="p-favorites">Favorites!</p>
      <hr className="hr-favorites" />
      <main className="main-home">
        {songs.map((song, i) => {
          return(
            <SongBox
              key={i}
              song={song}
              isFavorite={true}
            />
          )
        })}
      </main>
      <Footer/>
    </React.Fragment>
  )
}