import React, { Component } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Login from './pages/Login';
import Home from './pages/Home';
import Favorites from './pages/Favorites';

export default class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/home" element={ <Home/> } />
          <Route path="/favorites" element={ <Favorites/> } />
          <Route path="*" element={ <Login/> } />
        </Routes>
      </BrowserRouter>
    );
  }
}